<?php

Route::get('/', function () {
    return view('welcome');
});


$this->group(['prefix' => 'admin',], function(){
    Auth::routes();
});

Route::get('/home', 'HomeController@index')->name('home');
