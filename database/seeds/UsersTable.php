<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'   => 'Murilo Linares Mattioli',
            'email'  => 'admin@mattlm.com',
            'password'   => bcrypt('asdpo5xz')
        ]);
    }
}
